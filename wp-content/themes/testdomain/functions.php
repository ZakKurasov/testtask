<?php

function testdomain_theme_scripts_setup() {
	wp_enqueue_script('modal', get_stylesheet_directory_uri() . '/assets/js/modal.js', 0, 0, true);
}
add_action( 'wp_enqueue_scripts', 'testdomain_theme_scripts_setup' );

function testdomain_create_books_type() {
	register_post_type( 'books',
		array(
			'labels' => array(
				'name' => 'Books',
				'singular_name' => 'Books item',
				'menu_name' => 'Books'
			),
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'supports'              => array( 'title', 'editor', 'thumbnail' ),
			'rewrite'               => array( 'slug' => 'books' ),
			'has_archive'           => true,
			'hierarchical'          => true,
			'show_in_nav_menus'     => true,
			'capability_type'       => 'page',
			'query_var'             => true,
			'menu_icon'             => 'dashicons-admin-page',
		));
}

function testdomain_books_taxonomy() {
	register_taxonomy(
		'testdomain_books_taxonomy',  
		'books', 
		array(
			'hierarchical' => true,
			'label' => 'Genre', 
			'query_var' => true,
			'show_admin_column' => true,
			'rewrite' => array(
				'slug' => 'themes', 
				'with_front' => false 
			)
		)
	);
}

add_action( 'init', 'testdomain_books_taxonomy');
add_action( 'init', 'testdomain_create_books_type');

remove_action('woocommerce_single_product_summary','woocommerce_template_single_add_to_cart',30);
add_action('woocommerce_single_product_summary','woocommerce_template_single_add_to_cart',10);

remove_action('woocommerce_single_product_summary','woocommerce_template_single_rating',10);
add_action('woocommerce_single_product_summary','woocommerce_template_single_rating',30);

remove_action('woocommerce_single_product_summary','woocommerce_template_single_meta',40);