var modal = document.getElementById("footerModal");
var closeBtn = document.querySelector("#footerModal .close");

var contactBtn = document.querySelector("#contactButton");

contactBtn.onclick = function() {
	modal.style.display = "block";
}
closeBtn.onclick = function() {
	modal.style.display = "none";
}
document.onclick = function(e) {
	if (e.target == modal) {
		modal.style.display = "none";
	}
}

window.onload = function() {
	if (document.dir == "rtl") {
		var titles = document.querySelectorAll(".woocommerce-LoopProduct-link h3");
		titles.forEach(function(e) {
			e.innerHTML += "&#x200E;";
		})
		document.querySelectorAll("li.reviews_tab > a").forEach(function(e) { e.innerHTML += "&#x200E;"; })
	}
}