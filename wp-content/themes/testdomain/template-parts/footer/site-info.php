<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-info clear">
	<div class="footer-contacts">
		<p class="hide-xs">Call us: 0987654321</p>
		<p class="hide-xs">Email: <a href="mailto:testdomain@mail.to">testdomain@mail.to</a></p>
		<button href="tel:0987654321" class="show-xs">Call us</button>
		<button href="mailto:testdomain@mail.to" class="show-xs">Email</button>
		<button href="#" id="contactButton">Contact Us</button>
	</div>
	<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'twentyseventeen' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'twentyseventeen' ), 'WordPress' ); ?></a>
	<div id="footerModal" class="modal">
		<div class="modal-content">
			<span class="close">&times;</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, dolorum quos. Commodi blanditiis rerum architecto provident fuga ipsa adipisci sit id repellendus at rem repudiandae harum necessitatibus asperiores quod sed, neque repellat autem aliquam nemo veniam optio, dicta iure reiciendis. Eligendi possimus, ducimus velit. Deserunt quo animi maiores minus consectetur.</p>
		</div>
	</div>
</div><!-- .site-info -->
